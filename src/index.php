<?php

require_once("../vendor/autoload.php");

use App\Utilities\Factory;

$error = "";
$fileUploadError = "";
$data = [];
$path = "./data/data.json";
$upload_destination = "./data";
$fieldName = "data";

try
{

    $factory = new Factory();
    $helper = $factory->createHelper();

    /** Upload file if upload is triggered */

    if (isset($_POST["upload"]))
    {
        $status = $helper->uploadFile($fieldName, $upload_destination);
        if (is_array($status)) $fileUploadError = $status["error"];
    }

    /** Read ministry data from file */

    $ministryRepository = $factory->createMinistryRepository($helper);
    $ministryService = $factory->createMinistryService();

    $data = $ministryService->getMinistryData($path);
}
catch(\Exception $e)
{
    $error = $e->getMessage();
}

?>

<!DOCTYPE html>
<html lang="en">
    <head>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="utf-8">
        <title>MinistryApp - Ministries and Federal Parastatals</title>
        <link rel="stylesheet" href="./assets/css/reset.css">
        <link rel="stylesheet" href="./assets/vendor/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="./assets/vendor/jquery-ui/jquery-ui.min.css">
        <link rel="stylesheet" href="./assets/vendor/jquery-ui/jquery-ui.theme.min.css">
        <link rel="stylesheet" href="./assets/css/main.css">
    </head>
    <body>
        <div class="container">
            <div class="row col-md-offset-3 col-md-6 content">
                <div class="row">
                    <div class="col-md-12">
                        <h1 class="app-heading text-center"> Ministries and Federal Parastatals</h1>
                    </div>
                </div>

                <!-- Horizontal Rule -->
                <div class="row">
                    <div class="col-md-offset-4 col-md-4 main-rule"><hr class="main-hr"/></div>
                </div>

                <!-- File upload -->
                <div class="row upload">
                    <div class="col-md-12">
                        <form class="form-inline" action="#" method="post" enctype="multipart/form-data">
                            <div class="form-group">
                                <input type="file" name="data" class="form-control">
                            </div>
                            <div class="form-group">
                                <input type="submit" value="Upload Data" class="form-control btn btn-primary">
                            </div>
                            <div class="form-group">
                                <input type="hidden" name="upload" class="form-control">
                            </div>
                        </form>
                    </div>
                </div>

                <!-- Display error -->

                <?php

                    if ($fileUploadError !== "")
                    {

                ?>
                        <div class="row">
                            <div class='col-md-12'><p class='error'><?php echo $fileUploadError; ?></p></div>
                        </div>
                <?php

                    }
                ?>

                <?php

                    if ($error !== "" && $fileUploadError === "")
                    {

                ?>
                        <div class="row">
                            <div class='col-md-12'><p class='error'><?php echo $error; ?></p></div>
                        </div>
                <?php
                        return;
                    }
                ?>

                <!-- Horizontal Rule -->
                <div class="row">
                    <div class="col-md-12"><hr class="sub-hr-1"/></div>
                </div>

                <?php
                    if (isset($data["ministries"]) && count($data["ministries"]) > 0)
                    {
                ?>
                        <div class="row">
                <?php
                        foreach ($data["ministries"] as $ministry)
                        {
                ?>
                           <div class="col-md-12 ministry">

                                <div class="ministry-heading">
                                    <img src="./assets/images/coa.jpeg" width="40px" height="40px">
                                    <h2><?php echo $ministry["name"]; ?></h2>
                                </div>

                                <!-- Minister's Name -->
                                <p>
                                    <span class="bold">Minister</span>: <?php echo isset($ministry["minister"]) ? $ministry["minister"] : ""; ?>
                                    -
                                    <?php echo isset($ministry["origin"]) ? $ministry["origin"]: ""; ?> state
                                </p>

                                <!-- Minister of state -->
                                <?php
                                    echo
                                    isset($ministry["ministerOfState"])
                                    ?
                                    "<p><span class='bold'>Minister of State</span>:{$ministry['ministerOfState']}</p>"
                                    :
                                    "";
                                ?>
                                <div class="accordion">
                                    <h3 class="accord-header">More info</h3>
                                    <!-- Address -->
                                    <div>
                                        <p class="bold">Address:</p>
                                        <p class="address"><?php echo isset($ministry["address"]) ? $ministry["address"] : ""; ?></p>

                                        <!-- Web site -->
                                        <p><span class="bold">Web:</span>
                                                <?php
                                                    echo
                                                    isset($ministry["website"])
                                                    ?
                                                    "<a href='http://{$ministry['website']}' target='_blank'>{$ministry['website']}</a>"
                                                    :
                                                    "";
                                                ?>
                                        </p>
                                    </div>
                                </div>
                           </div>

                           <div class="col-md-12"><hr class="sub-hr-2"/></div>

                <?php
                        }
                ?>
                        </div>
                <?php
                    }
                ?>
            </div>
        </div>
        <script src="./assets/vendor/jquery/jquery.min.js"></script>
        <script src="./assets/vendor/jquery-ui/jquery-ui.min.js"></script>
        <script src="./assets/js/main.js"></script>
    </body>
</html>


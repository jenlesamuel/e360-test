<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Utilities\Helper;
use App\Repositories\MinistryRepository;

/**
 * MinistryRepository unit tests
 *
 * @author Jenle Samuel
 */

class MinistryRepositoryTest extends TestCase
{
    public function setUp()
    {
        $this->helperMock = $this->getMockBuilder(Helper::class)
                        ->getMock();

        $this->ministryRepository = new MinistryRepository($this->helperMock);
    }

    public function testGetMinistryDataFromFileReturnsArray()
    {
        $this->helperMock->expects($this->once())
             ->method("fileExists")
             ->will($this->returnValue(TRUE));

        $json = '{
                    "name": "Ministry of Justice",
                    "minister": "Abubakar Malami"
                 }';

        $this->helperMock->expects($this->once())
             ->method("getFileContent")
             ->will($this->returnValue($json));

        $actual = $this->ministryRepository->getMinistryDataFromFile("./any_path");
        $expected = json_decode($json, TRUE);

        $this->assertEquals($actual, $expected);

    }

}

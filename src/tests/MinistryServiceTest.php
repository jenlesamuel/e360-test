<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Utilities\Helper;
use App\Repositories\MinistryRepository;
use App\Services\MinistryService;

/**
 * MinistryService unit tests
 *
 * @author Jenle Samuel
 */

class MinistryServiceTest extends TestCase
{
    public function setUp()
    {
        $this->helperMock = $this->getMockBuilder(Helper::class)
                        ->getMock();

        $this->ministryRepositoryMock = $this->getMockBuilder(MinistryRepository::class)
                                             ->setConstructorArgs(array($this->helperMock))
                                             ->getMock();

        $this->ministryService = new MinistryService($this->ministryRepositoryMock);
    }

    public function testGetMinistryDataSuccess()
    {
        $this->ministryRepositoryMock->expects($this->once())
             ->method("getMinistryDataFromFile");

        $this->ministryService->getMinistryData('any_path');
    }

}

<?php

namespace App\Tests;

use PHPUnit\Framework\TestCase;
use App\Utilities\Helper;
use App\Utilities\Constants;

/**
 * Helper unit tests
 *
 * @author Jenle Samuel
 */

class HelperTest extends TestCase
{
    public function setUp()
    {
        $this->helper = new Helper();
    }

    public function testFileExistsThrowsExceptionIfFileNotFound()
    {

        $this->expectException(\Exception::class);
        $this->expectExceptionMessage(Constants::DATA_FILE_NOT_FOUND);
        $this->helper->fileExists("");
    }

    public function testFileExistsReturnsTrueIfFileExists()
    {
        $this->assertTrue($this->helper->fileExists(__FILE__));
    }

    public function testGetFileContentReturnsValidContent()
    {
        $str = $this->helper->getFileContent(__FILE__);
        $pos = strpos($str, "testGetFileContent()");

        $this->assertTrue(is_string($str));
        $this->assertFalse(($pos === FALSE));
    }
}

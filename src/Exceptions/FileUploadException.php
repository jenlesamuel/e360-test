<?php

namespace App\Exceptions;

/**
 * Exception thrown if file upload fails
 *
 * @author Jenle Samuel
 */

class FileUploadException extends \Exception
{
    public function __construct($message, $code=-1)
    {
        parent::__construct($message, $code);
    }
}
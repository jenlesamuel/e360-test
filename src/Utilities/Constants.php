<?php

namespace App\Utilities;

/**
 * Application constants
 *
 * @author Jenle Samuel
 */

class Constants
{
    const DATA_FILE_NOT_FOUND = "Data file not found";

    // File upload constants
    const FILE_OVERSIZE  = "File size is too large";
    const UNSUPPORTED_FILE_TYPE_ERROR = "Only json files are supported";
    const MAX_FILE_SIZE = 1000000; // ~1MB;
    const GENERIC_UPLOAD_ERROR = "Upload failed";
}
<?php

namespace App\Utilities;

use App\Utilities\Constants;

/**
 * Repo of helper functions
 *
 * @author Jenle Samuel
 */

class Helper
{
    /**
     * Uploads file
     *
     * @param  string  $fieldName The name of the file upload field.
     * @param  string  $dest The destination of uploaded file
     * @return mixed bool|array
     */

    public function uploadFile($fieldName, $dest)
    {
        // Check file size
        if ($_FILES[$fieldName]["size"] > Constants::MAX_FILE_SIZE)
            return ["error" => Constants::FILE_OVERSIZE];


        // Check file format
        $imageFileType = pathinfo(basename($_FILES[$fieldName]["name"]), PATHINFO_EXTENSION);
        if($imageFileType != "json")
            return ["error" => Constants::UNSUPPORTED_FILE_TYPE_ERROR];

        $destinationFile = $dest ."/"."data.json";
        if (! move_uploaded_file($_FILES[$fieldName]["tmp_name"], $destinationFile))
            return ["error" => Constants::GENERIC_UPLOAD_ERROR];

        return TRUE;
    }

    /**
     * Checks if a file exists
     *
     * @param  string  $path
     * @return bool
     * @throws \Exception if file does not exist
     */

    public function fileExists($path)
    {
        if (! file_exists($path)) throw new \Exception(Constants::DATA_FILE_NOT_FOUND);
        return TRUE;
    }

    /**
     * Gets the content of a file
     *
     * @return  string  $path
     */

    public function getFileContent($path)
    {
        return file_get_contents($path);
    }
}






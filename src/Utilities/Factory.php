<?php

namespace App\Utilities;

/**
 * Dependencies factory
 *
 * @author Jenle Samuel
 */

use App\Repositories\MinistryRepository;
use App\Services\MinistryService;
use App\Utilities\Helper;

class Factory
{
    /**
     * Creates an instance of a MinistryService
     *
     * @return \App\Services\MinistryService
     */

    public function createHelper()
    {
        return new Helper();
    }

    /**
     * Creates an instance of a MinistryRepository
     *
     * @return \App\Repositories\MinistryRepository
     */

    public function createMinistryRepository()
    {
        $helper = $this->createHelper();
        return new MinistryRepository($helper);
    }

    /**
     * Creates an instance of a MinistryService
     *
     * @return \App\Services\MinistryService
     */

    public function createMinistryService()
    {
        $ministryRepository = $this->createMinistryRepository();
        return new MinistryService($ministryRepository);
    }


}


<?php

namespace App\Repositories;

use App\Repositories\MinistryRepositoryInterface;
use App\Utilities\Constants;
use App\Utilities\Helper;

/**
 * Handles data related operations
 * Implementation of MinistryRepositoryInterface
 *
 * @author Jenle Samuel
 */

class MinistryRepository implements MinistryRepositoryInterface
{

    /**
     * Constructor
     *
     * @param Helper $helper
     */

    public function __construct(Helper $helper)
    {
        $this->helper = $helper;
    }

    /**
     * Reads ministry data from json file
     *
     * @param  string  $path The path to the json file
     * @return  mixed  array
     * @throws \Exception if file does not exist
     */

     public function getMinistryDataFromFile($path)
     {

        $this->helper->fileExists($path);

        $contents = $this->helper->getFileContent($path);
        $data = json_decode($contents, TRUE);

        return $data;
     }
}
<?php

namespace App\Repositories;

/**
 * Contract for ministry data access
 *
 * @author Jenle Samuel
 */

interface MinistryRepositoryInterface
{
    public function getMinistryDataFromFile($path);
}

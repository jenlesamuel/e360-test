<?php

namespace App\Services;

use App\Repositories\MinistryRepositoryInterface;

/**
 * Handles ministry related operations
 *
 * @author Jenle Samuel
 */

class MinistryService implements MinistryServiceInterface
{
    /**
     * Constructor
     *
     * @param MinistryRepositoryInterface $ministryRepository
     */

    public function __construct(MinistryRepositoryInterface $ministryRepository)
    {
        $this->ministryRepository = $ministryRepository;
    }

    /**
     * Gets ministry data from json file
     *
     * @param  string  $path The path to data file
     * @return array
     * @throws \Exception if file does not exist
     */

    public function getMinistryData($path)
    {
        // Attempt to get updated data
        return $this->ministryRepository->getMinistryDataFromFile($path);
    }
}

<?php

namespace App\Services;

/**
 * Contract for ministry related operations
 *
 * @author Jenle Samuel
 */

interface MinistryServiceInterface
{
    public function getMinistryData($path);
}
